#!/usr/bin/python3

from bs4 import BeautifulSoup
from multiprocessing import Value
import requests
import os
import time
import argparse
import sys
from progressbar import ProgressBar, SimpleProgress
from ctypes import c_int

class CrackMe:

    def __init__(self, name, link, location, lang, difficulty, platform):
        self.name = name
        self.link = os.path.join(link, location) + '.zip'
        self.lang = lang
        self.difficulty = difficulty
        self.platform = platform
        self.path = ''

    def make_dir(self, start):
        path = os.path.join(start, self.platform, self.lang, self.difficulty)
        if not os.path.isdir(path):
            os.system(f"mkdir -p {path}")
        self.path = path

    def get_zip(self):
        return os.path.join(self.path, self.name) + '.zip'

    def get_dir(self):
        return os.path.join(self.path, self.name)


def make_dir(directory):
    if not os.path.isdir(os.path.join(directory)):
        os.mkdir(os.path.join(directory))



def unzip_crack(crack):
    password = "crackmes.one"
    output_dir = crack.get_dir()
    zip_file = crack.get_zip()

    cmd = f"unzip -P {password} -o {zip_file} -d {output_dir} > /dev/null 2>&1"
    ret = os.system(cmd)

    if ret != 82: # wrong password
        password = "crackmes.de"
        cmd = f"unzip -P {password} -o {zip_file} -d {output_dir} > /dev/null 2>&1"
        ret = os.system(cmd)

    if ret == 0 or ret == 20992: # success
        cmd = f"rm {zip_file} > /dev/null 2>&1"
        os.system(cmd)


def sanitize_string(string):
    return string.strip().replace(' ', '_').replace('/', '_').replace('(','_').replace(')','_').replace("'","_").lower()

def prog_spinner(text):
    i = 0
    print(text + ': ', end='')
    while True:
        print("/-\|"[i % 4], end="\b")
        sys.stdout.flush()  #<- makes python print it anyway
        time.sleep(0.1)
        i += 1



def main():

    parser = parser = argparse.ArgumentParser()

    parser.add_argument('-f', '--file',
                        help='Read crackmes from file.')
    parser.add_argument('-u', '--url',
                        help='URL for crackmes',
                        default='https://crackmes.one/lasts')
    parser.add_argument('-d', '--directory',
                        help='Output directory',
                        default='crack_mes')
    parser.add_argument('-z', '--zip_location',
                        help='Location of zip files',
                        default='https://crackmes.one/static/crackme/')

    args = parser.parse_args(sys.argv[1:])

    data = None

    if args.file:
        f = open(args.file, 'r')
        data = f.read()
        f.close()
    else:
        r = requests.get(args.url)
        data = r.text

    soup = BeautifulSoup(data, features="html.parser")

    table = soup.find_all('table', class_='table table-striped')[0]

    make_dir(args.directory)

    cracks = []

    for row in table.find_all('tr', class_='text-center'):
        elements = row.find_all('td')
        name_data = elements[0].find('a', href=True)
        name = sanitize_string(name_data.get_text())
        location = name_data.attrs['href'].split('/')[-1]
        lang = sanitize_string(elements[2].get_text())
        diff = sanitize_string(elements[3].get_text())
        platform = sanitize_string(elements[4].get_text())

        crack = CrackMe(name, args.zip_location, location, lang, diff, platform)
        crack.make_dir(args.directory)

        cracks.append(crack)

    print('Downloading...')

    running  = Value(c_int, 0)
    prog = 0
    pbar = ProgressBar(widgets=[SimpleProgress()], maxval=len(cracks)).start()

    for crack in cracks:
        while (running.value > 10):
            pbar.update(prog)
            pass

        with running.get_lock():
            running.value += 1
            prog += 1

        if os.fork() == 0:
            zip_file = crack.get_zip()
            ret = os.system(f"wget - {crack.link} -O {zip_file} -o /dev/null")
            if ret == 1024:
                unzip_crack(crack)
            with running.get_lock():
                running.value -= 1
            quit()

    while (running.value > 0):
        pass

    pbar.finish()
    print('Done downloading.')


if __name__ == '__main__':
    main()
